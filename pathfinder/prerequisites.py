"""
.. module:: pathfinder.prerequisites
    :synopsis: Utilities for checking the status of prerequisites.

Prerequisites are stats and feature requirements.

Syntax: [not] <stat> <minimum>
    or: [not] <type> feat slot
    or: [not] <feature> [(<subtype>)]
    or: [not] <N><ord>-level <class>
    or: [not] restricted

Examples:
* restricted
* BAB 6
* Combat Expertise
* Weapon Focus (Shortsword)
* 9th-level Fighter
"""
import re

from pathfinder import feature_re

slot_re = re.compile(r'^(?P<type>.*) +feat +slot$')
lvl_re = re.compile(r'^(?P<level>\d+)(?:st|th|rd)-level +(?P<class>.*)$')
stat_re = re.compile(r'^(?P<stat>.*) +(?P<minimum>\d+)$')


def check(prerequisites, char):
    """
    Determine if the character satisfies the prerequisites.

    :param prerequisites: List of prerequisite expressions.
    :type prerequisites: list or tuple or set
    :param char: The character to test for compliance.
    :type char: pathfinder.Character

    :returns: A tuple of a bool pass/fail and the list of failures.
    :rtype: tuple
    """
    failures = [f for f in prerequisites if not check_prerequisite(f, char)]
    return len(failures) < 1, failures


def check_prerequisite(prerequisite, character):
    """Determine if the character satisfies a single prerequisite expression.

    :param prerequisite: List of prerequisite expressions.
    :type prerequisite: str
    :param character: The character to test for compliance.
    :type character: pathfinder.Character

    :rtype: bool
    """
    if prerequisite.startswith('not '):
        return not check_prerequisite(prerequisite[4:], character)
    if prerequisite == 'restricted':
        return False
    m = slot_re.match(prerequisite)
    if m:
        available_slots = character.available_feat_slots()
        slot_type = m.groupdict()['type']
        return slot_type in available_slots and available_slots[slot_type] > 0
    m = lvl_re.match(prerequisite)
    if m:
        level, class_name = m.groups()
        class_name = class_name.lower()
        level = int(level)
        for cls, lvl in character.classes.iteritems():
            if cls.name.lower() == class_name:
                return lvl >= level
        return False
    m = stat_re.match(prerequisite)
    if m:
        stat, minimum = m.groups()
        try:
            return character.get_stat(stat) >= int(minimum)
        except KeyError:
            return False
    m = feature_re.match(prerequisite)
    if m:
        name, subtype = m.groups()
        return character.has_feature(name, subtype)
