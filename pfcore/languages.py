from pathfinder.languages import Language


class Common(Language):
    name = 'Common'


class Dwarven(Language):
    name = 'Dwarven'
