from pathfinder.special_abilities import SpecialAbility


class Darkvision(SpecialAbility):
    name = 'Darkvision'
    description = "Extraordinary ability to see with no light source at all."


class LowLightVision(SpecialAbility):
    name = "Low-Light Vision"
    description = "Extraordinary ability to see well in even dim light."


class Scent(SpecialAbility):
    name = "Scent"
    description = "Extraordinary ability to detect creatures by their scent."
